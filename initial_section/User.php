<?php

class User
{
    public $firstName = 'Nitesh';
    public $lastName = 'Arora';
    private $job = 'Software engineer';
    protected $salary = 12000;

    public static $minimumPassword = 6;


    public function __construct($fname, $lname)
    {
        $this->firstName = $fname;
        $this->lastName = $lname;
    }

    public function getUserInfo()
    {
        $fullName = 'My name is :' . $this->firstName . ' ' . $this->lastName;
        return $fullName;
    }

    protected function jobInfo()
    {
        return $this->job;
    }

    public static function validation($password)
    {
        if (strlen($password) >= Seller::$minimumPassword) {
            return true;
        } else {
            return false;
        }
    }
}